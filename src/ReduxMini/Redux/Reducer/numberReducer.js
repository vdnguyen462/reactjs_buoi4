import { numberReducer } from "./rootReducer";
import { combineReducers } from "redux";

export const rootReducer_reduxMini = combineReducers({
    numberReducer: numberReducer,
})